package dkit.gd2;

public class TeamMain {

    public static void main(String[] args) {
        SoccerPlayer paudric = new SoccerPlayer("Paudric");
        HurlingPlayer niall = new HurlingPlayer("Niall");
        BasketballPlayer nathan = new BasketballPlayer("Nathan");

        Team<HurlingPlayer> truagh = new Team<>("Truagh");
        truagh.addPlayer(niall);
        //truagh.addPlayer(paudric);
        //truagh.addPlayer(nathan);

        Team<BasketballPlayer> theBullets = new Team<>("The Bullets");
        Team<BasketballPlayer> theBulls = new Team<>("The Bulls");
        theBullets.addPlayer(nathan);

        Team<SoccerPlayer> BoyneSideBlues = new Team<>("Boyne Side Blues");
        BoyneSideBlues.addPlayer(paudric);

        //theBullets.matchResult(BoyneSideBlues, 23, 17);

        //truagh.matchResult(theBullets, 21, 15);

        //theBullets.matchResult(theBulls, 20, 130);
        System.out.println(theBullets.getName() + ": " + theBullets.ranking());
        System.out.println(theBulls.getName() + ": " + theBulls.ranking());

        System.out.println(theBulls.compareTo(theBullets));

        League<Team<HurlingPlayer>> hurlingLeague = new League<>("Hurling league");
        League<Team<BasketballPlayer>> basketballLeague = new League<>("Basketball league");

        basketballLeague.add(theBullets);
        basketballLeague.add(theBulls);

        theBullets.matchResult(theBulls, 20, 130);

        basketballLeague.showLeagueTable();

        //System.out.println(truagh.numPlayers());
    }
}
