package dkit.gd2;

import java.util.ArrayList;
import java.util.Collections;

public class League<T extends Team> {

    /* Create a generic class to implement a league table for a sport. The class should allow teams to be added to the league and its should store a list of teams that belong to the league. You class should have a method to print the league table with the team with the most points at the top.
       Only teams of the same type should be able to be added to any instance of the league. The program should fail to compile of I try and add teams of different types to the same league
     */
    public String name;
    private ArrayList<T> league = new ArrayList<>();

    public League(String name) {
        this.name = name;
    }

    public boolean add(T team){
        if(league.contains(team)){
            return false;
        }
        league.add(team);
        return true;
    }

    public void showLeagueTable(){
        Collections.sort(league);
        for(T t : league){
            System.out.println(t.getName() + ": " + t.ranking());
        }
    }

}
